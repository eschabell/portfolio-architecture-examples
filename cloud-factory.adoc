= Cloud factory
Eric Lavarde @L1
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


This blueprint covers the architecture for deploying in an automated manner multiple instance of an OpenStack and Ceph based cloud,
following the principles of Infrastructure as Code.

Use case: Deploy multiple private clouds based on the same (infrastructure as) code using different parametrization (Ansible inventories).

Open the diagrams below directly in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your content.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/cloud-factory.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/cloud-factory.drawio?inline=false[[Download Diagram]]
--

--
image:logical-diagrams/cloud-factory-ld.png[350, 300]
image:schematic-diagrams/cloud-factory-details-sd.png[350, 300]
image:schematic-diagrams/cloud-factory-sd.png[350, 300]
image:detail-diagrams/cloud-factory-automation-orchestration.png[250, 200]
image:detail-diagrams/cloud-factory-monitoring-logging.png[250, 200]
image:detail-diagrams/cloud-factory-social-coding.png[250, 200]
image:detail-diagrams/cloud-factory-definitive-sw-library.png[250, 200]
--
