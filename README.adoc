= Red Hat Portfolio Architecture Examples
Eric Schabell @eschabell, Marty Wesley @mwesley_redhat, William Henry @ipbabble, Will Nix @tronik, Ishu Verma  @ishuverma, Marcos Entenza @makentenza
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify

== Overview
This repository provides examples of customer implementations using Red Hat product portfolio. These architectural blueprints can be used as is or adapted to meet the customer requirements.

The architectural diagrams are created using the open source tool draw.io.

The examples include high level overview in *Logical* diagrams, more in-depth view of how the various components and services work together in *Schematic* diagrams and core functions associated with a particular service or component in *Detailed* diagrams.

TIP: Learn how to use the draw.io tool to create portfolio architecture diagrams with https://gitlab.com/redhatdemocentral/portfolio-architecture-workshops[Portfolio Architecture Workshop]

TIP: Interested in the workflow to create, update, and push your blueprint project to this examples reposiotry? Try this https://redhatdemocentral.gitlab.io/portfolio-architecture-template[Beginners guide to deveoping an architecture blueprint]

== How to use
The architectural diagrams created using the diagram tool hosted online for free usage https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling[here]

To show varying levels of complexity, the library contains three styles of diagrams: logical, schematic, and detail.


You can download the example files and import them into the tool using:

  File -> Import from -> Device

Or you can click on the "Load Diagram" link and load the drawing directly into the tool.

Images that can be used for slides (click to enlarge an image):

== Blueprint Index

=== General
* link:cnd.adoc[Cloud native development]
* link:devsecops.adoc[DevSecOps for Cloud native development]
* link:omnichannel.adoc[Omnichannel customer experience]
* link:integrated-saas.adoc[Integrating with SaaS applications]
* link:continuous-remediation.adoc[Continuous Remediation]
* link:cloud-adoption.adoc[Cloud adoption]
* link:remote-management.adoc[Remote server management]
* link:cloud-factory.adoc[Cloud factory]
* link:batch-modernisation.adoc[Batch modernisation]

=== Vertical
* link:edge-ai-ml.adoc[Manufacturing]
* link:financial-services.adoc[Financial Services]
* link:retail.adoc[Retail]
* link:telco.adoc[Telco]

=== Product demo
* link:demos.adoc[Product demos]
